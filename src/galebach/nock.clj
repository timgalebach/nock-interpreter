(ns galebach.nock
  (:require
   [clojure.spec.alpha :as s]
   [orchestra.core :refer [defn-spec]]
   ))

(s/def ::atom (s/and int? #(>= % 0)))
(s/def ::cell (s/cat :car ::noun :cdr ::noun))
(s/def ::noun (s/or :atom ::atom :cell ::cell))

(comment (defn-spec nock ::noun [cell]))
